if [ -z "$1" ]
then
	DIR="."
else
	DIR=$1
fi

FILES=`ls -a $DIR`

touch _tmp_size_summarize.tmp
chmod 755 _tmp_size_summarize.tmp

for i in $FILES
do
	if [ $i != '.' ] ;
 	then
		if [ $i != '..' ] ;
		then
			du -hcsBM $DIR/$i >> _tmp_size_summarize.tmp
		fi
	fi
done

touch _tmp_size_summarize.out
chmod 755 _tmp_size_summarize.out

j=0
s=""
for i in `cat _tmp_size_summarize.tmp` 
do
	if [ $((j%4)) == 1 ] ;
	then
		s=$i
	else
		if [ $((j%4)) == 2 ] ;
		then
			echo -e $i '\t' $s >> _tmp_size_summarize.out
		fi
	fi
	j=$((j+1))
done
cat _tmp_size_summarize.out
rm -rf _tmp_size_summarize.out _tmp_size_summarize.tmp
